<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::make()->forceFill([
            'name' => 'jonas',
            'email' => "jonas@example.net",
            'password' => bcrypt('secret'),
            'email_verified_at' => now()->toDateTimeString()
        ])->save();
    }
}
