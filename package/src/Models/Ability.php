<?php

namespace JonasSlotte\Rbac\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use JonasSlotte\Rbac\Models\Traits\AbilityTrait;

class Ability extends Model
{
    use HasFactory, AbilityTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name'
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class)->using(AbilityRole::class);
    }
}
