<?php

namespace JonasSlotte\Rbac\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use JonasSlotte\Rbac\Models\Traits\RoleTrait;

class Role extends Model
{
    use HasFactory, RoleTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name'
    ];

    public function abilities()
    {
        return $this->belongsToMany(Ability::class)->using(AbilityRole::class);
    }

    public function hasAbility(string $ability)
    {
        return $this->abilities->contains('name', $ability);
    }
}
