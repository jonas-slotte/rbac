<?php

namespace JonasSlotte\Rbac\Providers;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;

class RbacServiceProvider extends ServiceProvider
{
    /**
     * The name of the configuration file/key
     *
     * @var string
     */
    protected $configName = "rbac";

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Default package config
        $this->mergeConfigFrom(
            __DIR__ . "/../config/{$this->configName}.php",
            $this->configName
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Filesystem $filesystem)
    {
        $this->publishes([
            __DIR__ . '/../database/migrations/create_rbac_tables.php' => $this->getMigrationFileName($filesystem),
        ], 'migrations');
        // Config file
        $this->publishes([
            __DIR__ . "/../config/{$this->configName}.php" => config_path("{$this->configName}.php")
        ]);
    }


    /**
     * Returns existing migration file if found, else uses the current timestamp.
     *
     * @param Filesystem $filesystem
     * @return string
     */
    protected function getMigrationFileName(Filesystem $filesystem): string
    {
        $timestamp = date('Y_m_d_His');

        return Collection::make($this->app->databasePath() . DIRECTORY_SEPARATOR . 'migrations' . DIRECTORY_SEPARATOR)
            ->flatMap(function ($path) use ($filesystem) {
                return $filesystem->glob($path . '*_create_rbac_tables.php');
            })->push($this->app->databasePath() . "/migrations/{$timestamp}_create_rbac_tables.php")
            ->first();
    }
}
