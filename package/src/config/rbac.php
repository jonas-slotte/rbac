<?php

use JonasSlotte\Rbac\Models\Ability;
use JonasSlotte\Rbac\Models\Role;
use JonasSlotte\Rbac\Models\AbilityRole;

return [
    'ability_model' => Ability::class,
    'role_model' => Role::class,
    'ability_role_model' => AbilityRole::class
];
