<?php

namespace JonasSlotte\Rbac\Tests\Feature;

use JonasSlotte\Rbac\Tests\TestCase;
use JonasSlotte\Rbac\Tests\TestSeeder;
use JonasSlotte\Rbac\Models\Ability;
use JonasSlotte\Rbac\Models\Role;

class RbacTest extends TestCase
{
    public function test_assign_ability_to_role()
    {
        $role = Role::create(["name" => 'therole']);
        $ability = Ability::create(["name" => 'theability']);
        $role->abilities()->attach($ability);
        $this->assertTrue($role->hasAbility('theability'));
    }
}
